#include <sstream>
#include <time.h>
#include <stdarg.h>
#include "logger.h"
#include "loggerexception.h"

namespace ravelogpp
{
	Logger GLogger;

	bool Logger::fileOutputEnabled = false;
	FILE* Logger::fw = NULL;

	void Logger::setFileLogging(const char* filename)
	{
		fw = fopen(filename, "w");

		if (!fw)
		{
			throw LoggerException(std::string("Cannot open output filename!!! : ") + std::string(filename));
		}

		fileOutputEnabled = true;
	}

	std::string Logger::printHeader()
	{
		std::stringstream sstream;

		time_t rawtime;
		struct tm * timeinfo;
		char buffer[80];

		time(&rawtime);
		timeinfo = localtime(&rawtime);

		strftime(buffer, 80, "%A %d.%m %Y %X", timeinfo);

		sstream << buffer << " ";

		return sstream.str();
	}

	void Logger::Trace(const char *prefix0, const char *prefix1, const char *prefix2, const char *fmt, ...)
	{
#ifdef LOGGER_ENABLED
#ifdef TRACE_ENABLED
		char buffer[2048];

		va_list argptr;
		va_start(argptr, fmt);
		vsprintf(buffer, fmt, argptr);
		va_end(argptr);

		std::stringstream sstream;
		sstream << "TRACE: " << printHeader() << prefix0 << " " << prefix1 << " " << prefix2 << ":::\"" << buffer << "\"\n";

		writeToFile(sstream.str().c_str());
		writeToConsole(sstream.str().c_str());
#endif
#endif
	}

	void Logger::Error(const char *fmt, ...)
	{

	}

	void Logger::Warning(const char *fmt, ...)
	{

	}

	void Logger::Fatal(const char *fmt, ...)
	{

	}

#ifdef LOGGER_ENABLED
	void Logger::writeToFile(const char*str)
	{
		if (fileOutputEnabled)
		{
			if (fw)
			{
				fprintf(fw, str);
			}
		}
	}

	void Logger::writeToConsole(const char*str)
	{
		fprintf(stdout, str);
	}
#else
	void Logger::writeToFile(const char*str)
	{

	}

	void Logger::writeToConsole(const char*str)
	{

	}
#endif
}