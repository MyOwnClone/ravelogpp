#pragma once
#include <stdio.h>
#include <string>

#define LOGGER_ENABLED

namespace ravelogpp
{
	class Logger
	{
	protected:
		static bool fileOutputEnabled;
		static FILE *fw;
	public:
		static void setFileLogging(const char* filename);
		static std::string printHeader();
		static void Trace(const char *prefix0, const char *prefix1, const char *prefix2, const char *fmt, ...);
		static void Error(const char *fmt, ...);
		static void Warning(const char *fmt, ...);
		static void Fatal(const char *fmt, ...);

		static std::string write(std::string msg);
		
		static void writeToFile(const char*str);
		static void writeToConsole(const char*str);
	};

	extern Logger GLogger;

#define TRACE(x, y) ravelogpp::GLogger.Trace(__FILE__, std::to_string(__LINE__).c_str(), __FUNCTION__, x, y) 
}
