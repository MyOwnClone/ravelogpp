#pragma once
#include <string>
#include <stdexcept>

namespace ravelogpp
{
	class LoggerException : public std::runtime_error
	{
	public:
		LoggerException(std::string message) : std::runtime_error(message) {};
	};
}